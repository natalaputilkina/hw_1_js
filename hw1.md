1. Як можна оголосити змінну у Javascript?

   змінну в Javascript можно оголосити 3 способами:

- var - способ застарілий, зараз не практикується;
- const - оголошується тоді, коли значення не змінюється в процесівиконання коду;
- let - оголошується тоді, коли значення потрібно переприсвоювати.

2. Що таке рядок (string) і як його створити (перерахуйте всі можливі варіанти)?

   string (рядок) із послідовних символів.
   варіанти рядків:

   - починається і закінчується одинарною лапкою (')
   - починається і закінчується подвійною лапкою (")
   - починається і закінчується обратною лапкою ('')

3. Як перевірити тип даних змінної в JavaScript?
   для того, щоб перевірити тип даних зміної, необхідно в консоль викликати оператор typeof та вказати ім'я змінної, яку потрібно перевірити.

4. Поясніть чому '1' + 1 = 11.
   - число у лапках '1' по правилам JS є символом у рядку, а не числом, тому числа не сладаються, а з`єднуються.
